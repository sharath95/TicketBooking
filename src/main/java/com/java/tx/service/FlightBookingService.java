package com.java.tx.service;

import com.java.tx.dto.FlightBookingAcknowledgement;
import com.java.tx.dto.FlightBookingRequest;
import com.java.tx.entity.PassengerInfo;
import com.java.tx.entity.PaymentInfo;
import com.java.tx.repository.PassengerInfoRepository;
import com.java.tx.repository.PaymentInfoRepository;
import com.java.tx.utils.PaymentUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FlightBookingService
{
    @Autowired
    private PassengerInfoRepository passengerInfoRepository;
    @Autowired
    private PaymentInfoRepository paymentInfoRepository;
    public FlightBookingAcknowledgement bookFlightTicket(FlightBookingRequest request)
    {
        FlightBookingAcknowledgement acknowledgement=null;
        PassengerInfo passengerInfo=request.getPassengerInfo();
        passengerInfo=passengerInfoRepository.save(passengerInfo);

        PaymentInfo paymentInfo=request.getPaymentInfo();
        PaymentUtils.validateCreditLimit(paymentInfo.getAccountNo(),passengerInfo.getFare());

        paymentInfo.setPassengerId(passengerInfo.getPID());
        paymentInfo.setAmount(passengerInfo.getFare());
        paymentInfoRepository.save(paymentInfo);
        return new FlightBookingAcknowledgement("SUCCESS",passengerInfo.getFare(), UUID.randomUUID().toString().split("-")[0],passengerInfo);
    }
}
