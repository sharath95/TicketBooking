package com.java.tx.utils;

public class InsufficientAmountException extends RuntimeException
{
  public InsufficientAmountException(String msg){
      super(msg);
  }
}
